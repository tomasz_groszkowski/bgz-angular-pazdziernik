import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "src/app/model/Album";
import { MusicService } from "../music.service";
import { Subscription, Subject, Observable } from "rxjs";
import { takeUntil, tap, catchError } from "rxjs/operators";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.css"],
  // viewProviders:[
  //   MusicService
  // ]
})
export class MusicSearchComponent implements OnInit {
  albums$ = this.searchService
    .getAlbums()
    .pipe(catchError(error => (this.message = error.message)));
  query$ = this.searchService.getQuery()

  message: string;

  constructor(private searchService: MusicService) {}

  search(query: string) {
    this.searchService.search(query);
  }

  ngOnInit() {}
}
