import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import {
  AbstractControl,
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  ValidatorFn,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  distinctUntilChanged,
  filter,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.css"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;

  @Output()
  queryChange = new EventEmitter<string>();

  @Input()
  set query(query: string) {
    this.queryForm.get("query").setValue(query, {
      emitEvent: false
    });
  }

  constructor(private fb: FormBuilder) {
    const censor = (badword: string): ValidatorFn => (
      control: AbstractControl
    ): ValidationErrors | null => {
      const hadError = (control.value as string).includes(badword);

      return hadError
        ? {
            censor: { badword }
          }
        : null;
    };

    const asyncCensor: AsyncValidatorFn = (control: AbstractControl) => {
      // return this.http.get('url'+control.value).pipe(map(resp => errors))

      const hasError = (control.value as string).includes("batman");

      return Observable.create(
        (observer: Observer<ValidationErrors | null>) => {
          const handler = setTimeout(() => {
            observer.next(
              hasError
                ? {
                    censor: { badword: "batman" }
                  }
                : null
            );
            observer.complete();
          }, 2000);

          return () => {
            clearTimeout(handler);
          };
        }
      );
    };

    this.queryForm = this.fb.group({
      query: this.fb.control(
        "",
        [Validators.required, Validators.minLength(3) /* , censor("batman") */],
        [asyncCensor]
      )
    });
    console.log(this.queryForm);

    const queryField = this.queryForm.get("query");

    const handleForm = (o: Observable<string>) =>
      o.pipe(
        debounceTime(400),
        distinctUntilChanged(),
        filter(query => query.length >= 3)
      );

    const value$ = queryField.valueChanges.pipe(handleForm);
    const status$ = queryField.statusChanges;

    const valid$ = status$.pipe(filter(status => status === "VALID"));

    const search$ = valid$.pipe(
      withLatestFrom(value$, (valid, value) => value)
    );

    search$.subscribe((query: string) => {
      this.search(query);
    });
  }

  search(query: string) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}
