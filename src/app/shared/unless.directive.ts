import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  Input,
  ViewRef
} from "@angular/core";
import { PlaylistsViewComponent } from "../playlists/playlists-view/playlists-view.component";
import { AppComponent } from '../app.component';

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
cache:ViewRef

  @Input()
  set appUnless(hide: boolean) {
    if (hide) {
      // this.vcr.clear()
      this.cache = this.vcr.detach()
    } else {
      if(this.cache){
        this.vcr.insert(this.cache,0)
      }else
      this.vcr.createEmbeddedView(this.tpl);
    }
  }
  
  constructor(
    // private cf: ComponentFactoryResolver,
    private tpl: TemplateRef<any>,
    private vcr: ViewContainerRef,
    private app:AppComponent
  ) {
    // console.log(app.register(this))
    // const f = cf.resolveComponentFactory(PlaylistsViewComponent)
    // const cr = this.vcr.createComponent(f)
    // cr.instance.playlists = []
  }
}
