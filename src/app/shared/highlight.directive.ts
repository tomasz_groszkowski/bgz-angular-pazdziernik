import {
  Directive,
  ElementRef,
  Attribute,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
  Renderer2,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host: {
  //   "[style.color]": "color",
  //   "[class.placki]": "true",
  //   "(mouseenter)": "activate($event)"
  // }
})
export class HighlightDirective implements OnInit, OnChanges {
  @Input("appHighlight")
  appHighlight: string;
  // set color(c){
  //   this.color = this.hover? this.appHighlight : ''
  // }

  @HostBinding("style.color")
  @HostBinding("style.border-left-color")
  get color(){
    // console.log('teraz' + Date.now())
    return this.hover? this.appHighlight : ''
  }

  @HostBinding('class.hover')
  hover = false;

  @HostListener("mouseenter",['$event.x'])
  activate(x:number) {
    this.hover = true;
  }

  @HostListener("mouseleave")
  deactivate() {
    this.hover = false;
  }

  constructor(
    private renderer: Renderer2,
    private elem: ElementRef<HTMLElement>
  ) {}

  ngOnInit() {
    // console.log("ngOnInit", this.color);
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("ngDoChanges", changes);
    if (changes["color"]) {
      // this.elem.nativeElement.style.color = this.color;
      // this.renderer.setStyle(this.elem.nativeElement,'color',this.color)
    }
  }

  ngDoCheck() {
    // console.log("ngDoCheck");
  }

  ngAfterContentInit() {
    // console.log("ngAfterContentInit");
  }
}

// console.log(HighlightDirective)

/* 
@Directive({
  selector: "[appHighlightSpecial]"
  // host: {
  //   "[style.color]": "color",
  //   "[class.placki]": "true",
  //   "(mouseenter)": "activate($event)"
  // }
})
export class HighlightDirectiveSpecial extends HighlightDirective {
  option = 'special'
}
 */