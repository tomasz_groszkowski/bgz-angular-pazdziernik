import { Injectable } from "@angular/core";
import { BehaviorSubject, of } from "rxjs";
import { Playlist } from "../model/playlist";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Hits",
      favourite: false,
      color: "#ff0000"
    },
    {
      id: 234,
      name: "Angular Top20",
      favourite: true,
      color: "#00ff00"
    },
    {
      id: 345,
      name: "Best of Angular",
      favourite: false,
      color: "#0000ff"
    }
  ]);
  selected = new BehaviorSubject<Playlist>(this.playlists.getValue()[0]);

  getPlaylists() {
    return this.playlists.asObservable();
  }

  getSelected() {
    return this.selected;
  }

  getPlaylist(id: Playlist["id"]) {
    return of(this.playlists.getValue().find(p => p.id == id));
  }

  select(id: Playlist["id"]) {
    this.selected.next(this.playlists.getValue().find(p => p.id == id));
  }

  update(playlist: Playlist) {
    this.playlists.next(
      this.playlists.getValue().map(p => (p.id == playlist.id ? playlist : p))
    );
    this.selected.next(playlist);
  }

  constructor() {}
}
