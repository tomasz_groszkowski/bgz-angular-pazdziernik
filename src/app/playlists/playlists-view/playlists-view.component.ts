import { Component, OnInit } from "@angular/core";
import { Playlist } from "../../model/playlist";
import { PlaylistsService } from "../playlists.service";
import { Router, ActivatedRoute } from "@angular/router";
import { switchMap, map } from "rxjs/operators";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.css"]
})
export class PlaylistsViewComponent implements OnInit {
  playlists$ = this.service.getPlaylists();
  // selected$ = this.service.getSelected();

  selected$ = this.route.paramMap.pipe(
    map(paramMap => parseInt(paramMap.get("id"))),
    switchMap(id => this.service.getPlaylist(id))
  );

  select(playlist: Playlist) {
    // this.service.select(playlist.id);
    this.router.navigate(["/playlists", playlist.id]);
  }

  save(playlist: Playlist) {
    this.service.update(playlist);
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {
    this.route.params.subscribe(console.log);
  }

  ngOnInit() {}
}
